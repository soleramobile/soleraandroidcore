package solera.android.core.presentation

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.*

typealias RunOnThread = () -> Unit
typealias RunOnThreadWithScope = (scope: CoroutineScope) -> Unit

open class BaseViewModel : ViewModel() {

    val isLoadingLiveData = MutableLiveData<Boolean>()
    val exceptionLiveData = MutableLiveData<Exception?>()

    fun executeIO(runOnThread: RunOnThread) : Job {
        return viewModelScope.launch {
            withContext(Dispatchers.IO) {
                try {
                    isLoadingLiveData.postValue(true)
                    runOnThread()
                    isLoadingLiveData.postValue(false)
                } catch (ex: Exception) {
                    ex.printStackTrace()
                    exceptionLiveData.postValue(ex)
                    isLoadingLiveData.postValue(false)
                }
            }
        }
    }

    fun executeIOCancellable(withDelayTime: Long = 200, runOnThreadWithScope: RunOnThreadWithScope) : Job {
        return viewModelScope.launch {
            withContext(Dispatchers.IO) {
                delay(withDelayTime)
                try {
                    isLoadingLiveData.postValue(true)
                    runOnThreadWithScope(this)
                    isLoadingLiveData.postValue(false)
                } catch (ex: Exception) {
                    ex.printStackTrace()
                    exceptionLiveData.postValue(ex)
                    isLoadingLiveData.postValue(false)
                }
            }
        }
    }
}