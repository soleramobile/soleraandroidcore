package solera.android.core.presentation

import androidx.annotation.MainThread
import androidx.lifecycle.MutableLiveData

class SingleLiveData<T> : MutableLiveData<T>() {
    @MainThread
    fun call() {
        postValue(null)
    }
}