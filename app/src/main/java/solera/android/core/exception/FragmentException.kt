package solera.android.core.exception

import solera.android.core.utils.Constants
import java.lang.Exception

class FragmentException(errorMessage: String = Constants.Error.Message.fragmentError) : Exception(errorMessage)