package solera.android.core.exception

import java.lang.Exception

class NoInternetConnectionException(var noInternetConnectionErrorMessage: String?) : Exception(noInternetConnectionErrorMessage)