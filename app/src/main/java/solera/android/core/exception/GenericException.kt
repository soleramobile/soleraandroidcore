package solera.android.core.exception

import solera.android.core.utils.Constants

class GenericException(
    var errorCode          : Int?    = Constants.defaultCode,
    var errorMessage       : String? = Constants.Error.Message.generalErrorMessage,
    var errorMessageDetail : String? = Constants.Error.Message.generalErrorMessage
) : Exception(errorMessageDetail)