package solera.android.core.exception

import java.lang.Exception

class NetworkException(var networkErrorMessage: String?) : Exception(networkErrorMessage)