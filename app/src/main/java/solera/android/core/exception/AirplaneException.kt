package solera.android.core.exception

import java.lang.Exception

class AirplaneException(var airplaneErrorMessage: String?) : Exception(airplaneErrorMessage)