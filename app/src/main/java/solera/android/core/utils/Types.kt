package solera.android.core.utils

import solera.android.core.exception.GenericException
import solera.android.core.exception.UnAuthorizeException
import java.lang.Exception

enum class BuildTypes(val rawName: String) {
    RELEASE("release"),
    DEBUG("debug"),
    DEVELOPMENT("development"),
    STAGING("staging");
}

enum class ErrorSessionType(val reasonMessage: String) {
    SESSION_EXPIRED("Sesión expirada"),
    SESSION_INVALID("Sesión invalida");
}

enum class ErrorResponse(val code: Int) {
    UNAUTORIZED(401);

    companion object {
        fun findException(code: Int, message: String, withException: (ex: Exception?) -> Unit) {
            withException.invoke(
                when (findError(code)) {
                    UNAUTORIZED -> UnAuthorizeException(message)
                    else -> null
                }
            )
        }

        fun returnException(code: Int?, message: String?, detail: String?) : Exception {
            return when (findError(code ?: 0)) {
                UNAUTORIZED -> UnAuthorizeException(message ?: String())
                else -> GenericException(
                    errorCode          = code    ?: Constants.Error.Code.generalErrorCode,
                    errorMessage       = message ?: Constants.Error.Message.generalErrorMessage,
                    errorMessageDetail = detail  ?: Constants.Error.Message.generalErrorMessage
                )
            }
        }

        private fun findError(code: Int) = values().find { it.code == code }
    }

}