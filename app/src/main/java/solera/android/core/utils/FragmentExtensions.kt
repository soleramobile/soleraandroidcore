package solera.android.core.utils

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import solera.android.core.exception.FragmentException

class FragmentAnimationModel(
    var mEnterAnim    : Int,
    var mExitAnim     : Int,
    var mPopEnterAnim : Int,
    var mPopExitAnim  : Int
)

fun FragmentManager?.addFragmentToNavigation(
    fragment: Fragment,
    tag: String,
    containerId: Int,
    currentFragment: Fragment? = null
) {
    try {
        this?.let {
            if (!it.fragmentIsAdded(fragment)) {
                it.beginTransaction().let { transaction ->
                    transaction.add(containerId, fragment, tag)
                    currentFragment?.let { cFragment -> transaction.hide(cFragment) }
                    transaction.addToBackStack(tag)
                    transaction.commit()
                }
            } else showExistingFragment(fragment, currentFragment)
        } ?: throw FragmentException()
    } catch (ex: Exception) {
        ex.printStackTrace()
    }
}

fun FragmentManager?.addFragmentToNavigationWithAnimation(
    fragment: Fragment,
    tag: String,
    containerId: Int,
    animations: FragmentAnimationModel,
    currentFragment: Fragment? = null
) {
    try {
        this?.let {
            if (!it.fragmentIsAdded(fragment)) {
                it.beginTransaction().let { transaction ->
                    transaction.setCustomAnimations(animations.mEnterAnim, animations.mExitAnim, animations.mPopEnterAnim, animations.mPopExitAnim)
                    transaction.add(containerId, fragment, tag)
                    currentFragment?.let { cFragment -> transaction.hide(cFragment) }
                    transaction.addToBackStack(tag)
                    transaction.commit()
                }
            } else showExistingFragment(fragment, currentFragment)
        } ?: throw FragmentException()
    } catch (ex: Exception) {
        ex.printStackTrace()
    }
}

fun FragmentManager?.addFragment(fragment: Fragment, tag: String, containerId: Int) {
    try {
        this?.let {
            it.beginTransaction().let { transaction ->
                transaction.add(containerId, fragment, tag)
                transaction.addToBackStack(null)
                transaction.commit()
            }
        } ?: throw FragmentException()
    } catch (ex: Exception) {
        ex.printStackTrace()
    }
}

fun FragmentManager?.moveBackToFirstFragment(currentFragment: Fragment?): Fragment? {
    try {
        this?.let {
            currentFragment?.let { cFragment ->
                if (it.fragments.size > 1 && it.fragments.first() != cFragment) {
                    it.beginTransaction().let { transaction ->
                        transaction.show(it.fragments.first())
                        transaction.hide(cFragment)
                        transaction.commit()
                    }
                    return it.fragments.first()
                }
            }
        }
    } catch (ex: Exception) {
        ex.printStackTrace()
    }
    return null
}

fun FragmentManager?.showExistingFragment(fragment: Fragment, currentFragment: Fragment? = null) {
    try {
        this?.let {
            it.beginTransaction().let { transaction ->
                transaction.show(fragment)
                currentFragment?.let { transaction.hide(currentFragment) }
                transaction.commit()
            }
        }
    } catch (ex: Exception) {
        ex.printStackTrace()
    }
}

fun FragmentManager?.fragmentIsAdded(fragment: Fragment): Boolean {
    return this?.let {
        return !it.fragments.isNullOrEmpty() && it.fragments.contains(fragment)
    } ?: false
}

fun FragmentManager.printFragments() {
    for (f in this.fragments) {
        println("HERE ${f::class.java.name}")
    }
}

fun AppCompatActivity.getFragmentByClass(classFragment: String): Fragment? {
    return this.supportFragmentManager.findFragmentByTag(classFragment)
}