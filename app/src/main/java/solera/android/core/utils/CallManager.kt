package solera.android.core.utils

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

class CallManager {

    companion object {
        fun call(context: Context, phone: String) {
            if (context is Activity) {
                if (validatePermission(context)) {
                    context.startActivity(Intent(Intent.ACTION_CALL).apply {
                        this.data = Uri.parse(Constants.IntentFormat.phone(phone))
                    })
                }
            } else {
                Log.d(Constants.problemLog, Constants.Error.Message.invalidValueType)
            }
        }

        private fun validatePermission(activity: Activity) : Boolean {
            return if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.CALL_PHONE), Constants.RequestCode.PHONE)
                false
            } else true
        }
    }

}