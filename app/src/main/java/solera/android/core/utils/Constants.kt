package solera.android.core.utils

import java.util.*

object Constants {
    const val empty              : String = ""
    const val defaultCode        : Int    = -1
    const val problemLog         : String = "PROBLEM_LOG"
    const val release_package    : String = "pe.solera.diners"//Set current project package
    const val app_playstore_url  : String = "https://play.google.com/store/apps/details?id=${release_package}&hl=en"
    const val app_maps_store_url : String = "http://play.google.com/store/apps/details?id=com.google.android.apps.maps"
    object Commons {
        const val DRAWABLE = "drawable"
        const val MIPMAP   = "mipmap"
        const val STRING   = "string"
    }
    object Packages {
        const val waze = "com.waze"
        const val whatsapp = "com.whatsapp"
        const val googleMaps = "com.google.android.apps.maps"
    }
    object RequestCode {
        const val PHONE : Int = 200
        const val SMS : Int = 201
        const val GPS : Int = 202
        const val GPS_ENABLED : Int = 203
    }
    object IntentFormat {
        fun phone(phone: String) = "tel:${phone.trim()}"
    }
    object BuildTypes {
        const val release     : String = "release"
        const val debug       : String = "debug"
        const val development : String = "development"
    }
    object Error {
        object Code {
            const val generalErrorCode : Int = -1
            const val unAuthorizeCode  : Int = 401
        }
        object Message {
            const val nullErrorMessage     : String = "Contenido inválido."
            const val generalErrorMessage  : String = "Ha ocurrido un error desconocido."
            const val errorNotification    : String = "Error Notification"
            const val invalidValueType     : String = "Tipo de dato inválido"
            const val fragmentError        : String = "Ha ocurrido un error en la transacción de fragments"
            const val cantShowKeyboard     : String = "No se ha podido mostrar el teclado seguro"
            const val invalidUrl           : String = "Invalid URL"
        }
        object View {
            const val cantFindView         : String = "No se pudo encontrar view"
            const val cantModifyView       : String = "No se pudo modificar view"
            const val cantModifyEditText   : String = "No se pudo modificar editText"
            const val cantModifyMap        : String = "No se pudo modificar mapa"
            const val cantShowDialog       : String = "No se pudo mostrar dialog"
            const val cantModifyBackground : String = "No se pudo modificar background"
            const val cantModifyImage      : String = "No se pudo modificar imagen"
            const val cantModifyTextColor  : String = "No se pudo modificar textColor"
            const val cantModifyText       : String = "No se pudo modificar text"
            const val cantDelayButtonClick : String = "No se pudo bloquear el botón"
            const val cantModifyTopBar     : String = "No se pudo modificar topBar"
            const val cantModifyBottomNav  : String = "No se pudo modificar Bottom nav view"
        }
    }
    object Server {
        const val BASE_URL      : String = "BASE_URL"
        const val BEARER        : String = "Bearer "
        const val PLATFORM      : String = "Android"
        const val TIMEOUT       : Long   = 60L
        const val AUTHORIZATION : String = "Authorization"
        const val X_OS          : String = "x-os"
        const val X_VERSION     : String = "x-version"
        const val X_APP         : String = "x-app"
        const val X_DENSITY     : String = "x-density"
        const val X_WIDTH       : String = "x-width"
        const val X_HEIGHT      : String = "x-height"
        const val CACHE_SIZE    : Long   = 10485760
    }
    object Locale {
        val AmountsLocale = Locale("en", "US") // Manage amounts formats with this locale
        val Spanish       = Locale("es")
        val English       = Locale("us")
    }
    object Currency {
        const val pen : String = "S/"
        const val usd : String = "$"
    }
    object DatePattern {
        const val ddMMyyyy = "dd-MM-yyyy"
        const val dayAndMonth = "dd MMMM"
        const val dayAndMonthShort = "dd MMM"
        const val dayMonthYear = "dd MMMM yyyy"
    }
}