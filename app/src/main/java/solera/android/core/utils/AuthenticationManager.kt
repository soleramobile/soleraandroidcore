package solera.android.core.utils

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import solera.android.core.BuildConfig
import solera.android.core.R
import java.util.concurrent.Executor

//Never call from dialog fragment, just activities

class AuthenticationManager(var activity: AppCompatActivity?, var listener: AuthenticationHandler? = null) {

    private lateinit var executor             : Executor
    private lateinit var biometricPrompt      : BiometricPrompt
    private lateinit var biometricPromptInfo  : BiometricPrompt.PromptInfo

    enum class AuthenticationStatusType(val stringId: Int, val rawValue: String) {
        CAN_AUTHENTICATE(R.string.can_authenticate, "CAN_AUTHENTICATE"),
        NO_HARDWARE(R.string.no_hardware, "NO_HARDWARE"),
        UNAVAILABLE(R.string.unavailable, "UNAVAILABLE"),
        NO_FINGERPRINTS(R.string.no_fingerprints, "NO_FINGERPRINTS"),
        UNKNOWN_STATUS(R.string.unknown_status, "UNKNOWN_STATUS");

        companion object {
            fun findByRawValue(rawValue: String) = values().find { it.rawValue == rawValue } ?: UNKNOWN_STATUS
        }
    }

    interface AuthenticationHandler {
        fun onError(errorCode: Int, errorString: String)
        fun onSucceeded(result: BiometricPrompt.AuthenticationResult)
        fun onFailed()
    }

    companion object {
        fun validateIfCanAuthenticateWithBiometrics(context: Context?) : AuthenticationStatusType {
            return context?.let {
                val biometricManager = BiometricManager.from(it)
                return when (biometricManager.canAuthenticate()) {
                    BiometricManager.BIOMETRIC_SUCCESS              -> AuthenticationStatusType.CAN_AUTHENTICATE
                    BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE    -> AuthenticationStatusType.NO_HARDWARE
                    BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE -> AuthenticationStatusType.UNAVAILABLE
                    BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED  -> AuthenticationStatusType.NO_FINGERPRINTS
                    else -> AuthenticationStatusType.UNKNOWN_STATUS
                }
            } ?: AuthenticationStatusType.UNKNOWN_STATUS
        }
    }

    init { this.initializeAutheticationMethods() }

    private fun initializeAutheticationMethods() {
        try {
            activity?.let {
                this.executor = ContextCompat.getMainExecutor(it)
                this.biometricPrompt = BiometricPrompt(it, executor, object : BiometricPrompt.AuthenticationCallback() {
                    override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                        super.onAuthenticationError(errorCode, errString)
                        listener?.onError(errorCode, it.getString(R.string.authentication_error, errString.trim().toString()))
                    }

                    override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                        super.onAuthenticationSucceeded(result)
                        listener?.onSucceeded(result)
                    }

                    override fun onAuthenticationFailed() {
                        super.onAuthenticationFailed()
                        listener?.onFailed()
                    }
                })
                this.biometricPromptInfo = BiometricPrompt.PromptInfo.Builder()
                    .setTitle(BuildConfig.APP_NAME)
                    .setSubtitle(it.getString(R.string.put_finger_to_continue))
                    .setNegativeButtonText(it.getString(R.string.cancel))
                    .setConfirmationRequired(true)
                    .build()
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    fun showAuthenticationDialog(statusType: AuthenticationStatusType) {
        when(statusType) {
            AuthenticationStatusType.CAN_AUTHENTICATE -> this.biometricPrompt.authenticate(this.biometricPromptInfo)
            else -> {
                this.activity?.let {
                    it.showMaterialDialog(message = it.getString(statusType.stringId), acceptBtnMsg = activity?.getString(R.string.accept))
                }
            }
        }
    }
}