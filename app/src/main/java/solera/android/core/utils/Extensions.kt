package solera.android.core.utils

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.content.res.Resources
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.text.InputFilter
import android.util.DisplayMetrics
import android.util.Log
import android.util.Patterns
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.animation.AccelerateInterpolator
import android.view.animation.AlphaAnimation
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.core.app.ActivityOptionsCompat
import androidx.core.content.ContextCompat
import androidx.core.graphics.ColorUtils
import androidx.core.view.get
import androidx.core.view.size
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.viewpager2.widget.ViewPager2
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.material.bottomnavigation.BottomNavigationItemView
import com.google.android.material.bottomnavigation.BottomNavigationMenuView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import solera.android.core.BuildConfig
import solera.android.core.R
import solera.android.core.exception.*
import solera.android.core.utils.Constants.Error.Message.generalErrorMessage
import java.io.*
import java.lang.reflect.Type
import java.text.SimpleDateFormat
import java.util.*

fun Exception?.getMessageException() : String {
    return this?.let {
        return when(this) {
            is GenericException     -> (it as GenericException).errorMessage          ?: generalErrorMessage
            is NetworkException     -> (it as NetworkException).networkErrorMessage   ?: generalErrorMessage
            is AirplaneException    -> (it as AirplaneException).airplaneErrorMessage ?: generalErrorMessage
            is NoInternetConnectionException -> (it as NoInternetConnectionException).noInternetConnectionErrorMessage ?: generalErrorMessage
            is UnAuthorizeException -> (it as UnAuthorizeException).errorSesionMsg
            else -> it.message ?: generalErrorMessage
        }
    } ?: generalErrorMessage
}

fun <T> Context?.getJsonFromResource(fileName: String, type: Type) : T? {
    return this?.let {
        return Gson().fromJson(it.assets.open(fileName).bufferedReader().use { buffer ->
            buffer.readText()
        }, type)
    }
}

fun <T> String?.fromJsonOrNull() : T? {
    return Gson().fromJson(this ?: return null, object : TypeToken<T>(){}.type)
}

fun Context?.parseJsonFromResource(fileName: String, json: (json: String) -> Unit){
    this?.let {
        try {
            json.invoke(it.readFromJsonFile(fileName))
        } catch (ex: Exception) {
            ex.printStackTrace()
            null
        }
    }
}

fun Context.readFromJsonFile(fileName: String) : String {
    return try {
        val inputStream: InputStream = this.assets.open(fileName)
        val inputStreamReader = InputStreamReader(inputStream)
        val bufferedReader = BufferedReader(inputStreamReader)
        val stringBuilder = StringBuilder()
        var receiveString : String
        while (bufferedReader.readLine().let { receiveString = it ?: ""; it != null }) {
            stringBuilder.append(receiveString)
        }
        inputStream.close()
        stringBuilder.toString()
    } catch (ex: Exception) {
        ex.printStackTrace()
        String()
    }
}

fun View?.delayClickState(timeMillis: Long = 1000) {
    this?.let {
        it.apply { it.isEnabled = false }
        Handler().postDelayed({
            it.apply { it.isEnabled = true }
        }, timeMillis)
    } ?: run {
        Log.d(Constants.problemLog, Constants.Error.View.cantDelayButtonClick)
    }
}

fun Context?.showToast(message: String) {
    this ?: return
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun ViewGroup.inflate(layoutId: Int, attachToRoot: Boolean = false): View =
    LayoutInflater.from(context).inflate(layoutId, this, attachToRoot)

fun ImageView.loadImage(url: String) = Glide.with(this).load(url).into(this)

fun ImageView.loadImageRound(url: String) =
    Glide.with(this).load(url).apply(RequestOptions.circleCropTransform()).into(this)

fun SwipeRefreshLayout.startRefreshing() {
    isRefreshing = true
}

fun SwipeRefreshLayout.stopRefreshing() {
    isRefreshing = false
}

//Use only from Activities, don't use from Fragment (with getActivity) or from Dialog/DialogFragment

fun Activity.hideKeyboard() {
    val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    val view = currentFocus ?: View(this)
    imm.hideSoftInputFromWindow(view.windowToken, 0)
    window.decorView
}

fun View.showKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)
}

//Use everywhere except from Activity (Custom View, Fragment, Dialogs, DialogFragments)

fun View.hideKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(windowToken, 0)
}


fun Context?.getDrawableByResourceName(name: String?): Drawable? {
    return this?.let {
        it.resources.getIdentifier(name,
            Constants.Commons.DRAWABLE, this.packageName).let { identifier ->
            return ContextCompat.getDrawable(this, identifier)
        }
    }
}

fun Context?.getStringByResourceName(name: String?) : String {
    return this?.let {
        if (name.isNullOrEmpty()) {
            return String()
        }
        it.resources.getIdentifier(name, Constants.Commons.STRING, it.packageName).let { identifier ->
            return it.getString(identifier)
        }
    } ?: String()
}

fun Activity.getTransitionAnimationOptions(view: View, transitionName: String): Bundle? {
    return ActivityOptionsCompat.makeSceneTransitionAnimation(this, view, transitionName).toBundle()
}

//Default dialogs

fun Context?.showMaterialDialog(title: String? = null, message: String? = null, acceptBtnMsg: String? = "", showNegativeBtn: Boolean = false, action: ((action: Boolean) -> Unit)? = null) {
    this?.let {
        MaterialAlertDialogBuilder(it).let { builder ->
            builder.setCancelable(false)
            builder.setIcon(ContextCompat.getDrawable(it, R.drawable.ic_logo_diners))//Set current project icon
            builder.setTitle(title ?: BuildConfig.APP_NAME)
            builder.setMessage(message ?: Constants.empty)
            builder.setPositiveButton(acceptBtnMsg ?: it.getString(R.string.accept)) { dialog, _ ->
                dialog.dismiss()
                action?.invoke(true)
            }
            if (showNegativeBtn) { builder.setNegativeButton(it.getText(R.string.cancel)) { dialog, _ ->
                dialog.dismiss()
                action?.invoke(false)
            } }
            builder.show()
        }
    } ?: return
}

//Dialog that receives an array of options to select

fun Context?.showMaterialDialogWithOptions(
    items: Array<String>,
    title: String? = null,
    showNegativeBtn: Boolean = false,
    itemSelected: Int = -1,
    selectedOption: ((option: String, index: Int, positive: Boolean) -> Unit)?) {
    var selection : Int = itemSelected
    MaterialAlertDialogBuilder(this ?: return).let { builder ->
        builder.setCancelable(false)
        builder.setTitle(title ?: BuildConfig.APP_NAME)
        builder.setSingleChoiceItems(items, selection) { _, which ->
            selection = which
        }
        builder.setPositiveButton(this.getText(R.string.select)) { dialog, _ ->
            if (selection >= 0) {
                selectedOption?.invoke(items[selection], selection, true)
                dialog.dismiss()
            }
        }
        if (showNegativeBtn) {
            builder.setNegativeButton(this.getText(R.string.cancel)) { dialog, _ ->
                selectedOption?.invoke("", selection, false)
                dialog.dismiss()
            }
        }
        builder.show()
    }
}

//Custom dialogs

fun Activity?.showDialog(layoutId: Int, func: Dialog.() -> Unit) {
    this?.let {
        try {
            Dialog(it).apply {
                this.requestWindowFeature(Window.FEATURE_NO_TITLE)
                this.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                this.setCancelable(false)
                this.setContentView(layoutId)
                this.func()
                this.show()
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.d(Constants.problemLog, ex.getMessageException())
        }
    } ?: run {
        Log.d(Constants.problemLog, Constants.Error.View.cantShowDialog)
    }
}

//App personalization

val Int.dp : Int
    get() = (this * Resources.getSystem().displayMetrics.density + 0.5f).toInt()

val Float.dp : Int
    get() = (this * Resources.getSystem().displayMetrics.density + 0.5f).toInt()

val Double.dp : Int
    get() = (this * Resources.getSystem().displayMetrics.density + 0.5f).toInt()

fun Float.toDp(metrics: DisplayMetrics): Float {
    return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, this, metrics)
}

//Returns do values
enum class SizeType {HEIGHT, WIDTH;}

fun Context?.screenSize(sizeType: SizeType) : Int {
    return this?.let {
        val displayMetrics = it.resources.displayMetrics
        return if (sizeType == SizeType.HEIGHT) displayMetrics.heightPixels else displayMetrics.widthPixels
    } ?: 0
}

fun Float?.rangeValue(input_min: Float, input_max: Float, output_min: Float, output_max: Float): Float {
    return this?.let {
        return (it - input_min) * (output_max - output_min) / (input_max - input_min) + output_min
    } ?: 0f
}

fun Activity?.setTopBarColor(colorIdOrHex: Any) {
    this?.let {
        try {
            var bgColor = 0
            when (colorIdOrHex) {
                is Int    -> bgColor = ContextCompat.getColor(it, colorIdOrHex)
                is String -> bgColor = Color.parseColor(colorIdOrHex)
            }
            it.window.statusBarColor = bgColor
            it.window.decorView.systemUiVisibility = if (ColorUtils.calculateLuminance(bgColor) > 0.5) View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR else 0
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.d(Constants.problemLog, ex.getMessageException())
        }
    } ?: run {
        Log.d(Constants.problemLog, Constants.Error.View.cantModifyTopBar)
    }
}

//View personalization

fun View?.setBackgroundDrawableById(drawableId: Int) {
    this?.let {
        try {
            this.background = ContextCompat.getDrawable(it.context, drawableId)
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.d(Constants.problemLog, ex.getMessageException())
        }
    } ?: run {
        Log.d(Constants.problemLog, Constants.Error.View.cantModifyBackground)
    }
}

fun ImageView?.setImageDrawableById(idDrawable: Int) {
    this?.let {
        try {
            this.setImageDrawable(ContextCompat.getDrawable(this.context, idDrawable))
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.d(Constants.problemLog, ex.getMessageException())
        }
    } ?: run {
        Log.d(Constants.problemLog, Constants.Error.View.cantModifyImage)
    }
}

fun View?.setAnyBackgroundColor(colorIdOrHex: Any) {
    this?.let {
        try {
            when (colorIdOrHex) {
                is Int    -> this.setBackgroundColor(ContextCompat.getColor(it.context, colorIdOrHex))
                is String -> this.setBackgroundColor(Color.parseColor(colorIdOrHex))
                else      -> this.setBackgroundColor(Color.BLUE)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.d(Constants.problemLog, ex.getMessageException())
        }
    } ?: run {
        Log.d(Constants.problemLog, Constants.Error.View.cantModifyBackground)
    }
}

fun View?.setAnyBackgroundTintColor(colorIdOrHex: Any) {
    this?.let {
        try {
            when (colorIdOrHex) {
                is Int    -> this.backgroundTintList = ContextCompat.getColorStateList(this.context, colorIdOrHex)
                is String -> this.background.setTint(Color.parseColor(colorIdOrHex))
                else      -> this.setBackgroundColor(Color.BLUE)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.d(Constants.problemLog, ex.getMessageException())
        }
    } ?: run {
        Log.d(Constants.problemLog, Constants.Error.View.cantModifyBackground)
    }
}

fun View?.setGradientBackgroundColor(
    hexColorTop: String,
    hexColorBottom: String,
    orientation: GradientDrawable.Orientation = GradientDrawable.Orientation.TOP_BOTTOM
) {
    this?.let {
        try {
            this.background = GradientDrawable().apply {
                this.orientation = orientation
                this.colors = intArrayOf(Color.parseColor(hexColorTop), Color.parseColor(hexColorBottom))
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.d(Constants.problemLog, ex.getMessageException())
        }
    } ?: run {
        Log.d(Constants.problemLog, Constants.Error.View.cantModifyBackground)
    }
}

fun TextView?.setAnyTextColor(colorIdOrHex: Any) {
    this?.let {
        try {
            when (colorIdOrHex) {
                is Int    -> this.setTextColor(ContextCompat.getColor(it.context, colorIdOrHex))
                is String -> this.setTextColor(Color.parseColor(colorIdOrHex))
                else      -> Log.d(Constants.problemLog, Constants.Error.Message.invalidValueType)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.d(Constants.problemLog, ex.getMessageException())
        }
    } ?: run {
        Log.d(Constants.problemLog, Constants.Error.View.cantModifyTextColor)
    }
}

fun TextView?.setAnyText(text: Any) {
    this?.let {
        try {
            when (text) {
                is Int    -> this.text = context.getString(text)
                is String -> this.text = text
                else      -> Log.d(Constants.problemLog, Constants.Error.Message.invalidValueType)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.d(Constants.problemLog, ex.getMessageException())
        }
    } ?: run {
        Log.d(Constants.problemLog, Constants.Error.View.cantModifyText)
    }
}

//ViewPager2

fun ViewPager2?.removeOverScroll() {
    this?.let {
        if (it.getChildAt(0) is RecyclerView) {
            (it.getChildAt(0) as RecyclerView).overScrollMode = RecyclerView.OVER_SCROLL_NEVER
        } else {
            Log.d(Constants.problemLog, Constants.Error.Message.invalidValueType)
        }
    } ?: run {
        Log.d(Constants.problemLog, Constants.Error.View.cantModifyView)
    }
}

//Google maps

fun GoogleMap?.updateMapCameraPosition(latLng: LatLng?, zoom: Float) {
    this?.let {
        try {
            this.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng ?: return, zoom))
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.d(Constants.problemLog, ex.getMessageException())
        }
    } ?: run {
        Log.d(Constants.problemLog, Constants.Error.View.cantModifyMap)
    }
}

//BottomNavigationView

fun BottomNavigationView?.selectItem(index: Int) {
    this?.let {
        it.getChildAt(0)?.let { child ->
            if (child is BottomNavigationMenuView) {
                if (child.size >= index) {
                    (child[index] as BottomNavigationItemView).performClick()
                }
            }
        }
    } ?: run {
        Log.d(Constants.problemLog, Constants.Error.View.cantModifyBottomNav)
    }
}

//Seekbar

fun SeekBar?.disableDraggingFunction() {
    this?.let{
        it.setOnTouchListener { _, _ ->
            true
        }
    }
}

//String format extensions

fun EditText?.setMaxLength(max: Int) {
    this?.let {
        it.filters = arrayOf(InputFilter.LengthFilter(max))
    } ?: run {
        Log.d(Constants.problemLog, Constants.Error.View.cantModifyEditText)
    }
}

val Date.ddMMMyyyy : String
    get() = (SimpleDateFormat("dd/MMM/yyyy", Locale.getDefault()).format(this)
        .replace(".", "")).upperCase(4)

fun dateByPattern(date: Date?, pattern: String) : String {
    date ?: return String()
    val formatter = SimpleDateFormat(pattern, Constants.Locale.Spanish)
    return formatter.format(date).capitalizeAllWords()
}

fun String?.upperCase(position: Int) : String {
    return this?.replaceRange(position - 1, position, this.substring(position - 1, position).toUpperCase(Locale.US))
        ?: Constants.empty
}

fun Double.roundToIntString() : String {
    return if (this.rem(1).equals(0.0)) this.toInt().toString() else this.toString()
}

fun String?.capitalizeWord() : String {
    return this?.let {
        val line = it.toLowerCase(Constants.Locale.Spanish)
        return if (line.isNotEmpty()) {
            Character.toUpperCase(line[0] ) + line.substring(1)
        } else ""
    } ?: ""
}

fun String?.capitalizeAllWords() : String {
    return this?.let {
        val line = it.split(" ")
        var returnValue = String()
        for (value in line) {
            returnValue = returnValue + " " + value.capitalizeWord()
        }
        return returnValue.trim()
    } ?: ""
}

//Validations

fun String.isEmail(): Boolean {
    val pattern = Patterns.EMAIL_ADDRESS
    return pattern.matcher(this).matches()
}

fun Activity?.sendToPlayStoreForUpdate() {
    this?.let {
        it.applicationContext.startActivity(
            Intent(Intent.ACTION_VIEW, Uri.parse(Constants.app_playstore_url)).apply {
                this.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            })
        it.finish()
    }
}

fun Activity.validateGpsEnabled(isActive: (active: Boolean) -> Unit) {
    try {
        val lm : LocationManager? = this.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        isActive.invoke(lm?.isProviderEnabled(LocationManager.GPS_PROVIDER) == true)
    } catch (ex: Exception) {
        ex.printStackTrace()
    }
}

//Animations

fun View.fadeZeroToOne() {
    val fadeOut = AlphaAnimation(0f, 1f)  // the 1, 0 here notifies that we want the opacity to go from opaque (1) to transparent (0)
    fadeOut.interpolator = AccelerateInterpolator()
    fadeOut.startOffset = 500 // Start fading out after 500 milli seconds
    fadeOut.duration = 300
    this.animation = fadeOut
}

fun View.fadeOneToZero() {
    val fadeOut = AlphaAnimation(1f, 0f) // the 1, 0 here notifies that we want the opacity to go from opaque (1) to transparent (0)
    fadeOut.interpolator = AccelerateInterpolator()
    fadeOut.startOffset = 500 // Start fading out after 500 milli seconds
    fadeOut.duration = 300
    this.animation = fadeOut
}

fun Activity.validateMapIntent(location: Pair<Double, Double>) {
    val packageManager = this.packageManager
    val geoMaps = Uri.parse("google.navigation:q=${location.first},${location.second}")
    val geoWaze = Uri.parse("waze://?ll=${location.first}, ${location.second}&navigate=yes")
    val intentWaze = Intent(Intent.ACTION_VIEW, geoWaze)
    intentWaze.setPackage(Constants.Packages.waze)
    val intentMap = Intent(Intent.ACTION_VIEW, geoMaps)
    intentMap.setPackage(Constants.Packages.googleMaps)

    try {
        if (intentMap.resolveActivity(packageManager) != null) {
            val title = this.getString(R.string.start_nagivation)
            val chooserIntent = Intent.createChooser(intentMap, title)
            if (intentWaze.resolveActivity(packageManager) != null) {
                val arr = arrayOfNulls<Intent>(1)
                arr[0] = intentWaze
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, arr)
                this.startActivity(chooserIntent)
            } else {
                this.startActivity(chooserIntent)
            }
        } else {
            Toast.makeText(this, this.getString(R.string.no_maps), Toast.LENGTH_SHORT).show()
            this.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(Constants.app_maps_store_url)))
        }
    } catch (e: Exception) {
        Toast.makeText(this, this.getString(R.string.no_maps), Toast.LENGTH_SHORT).show()
        this.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(Constants.app_maps_store_url)))
    }
}