package solera.android.core.utils

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment

typealias SmsPermissionCorrect = (isCorrect: Boolean) -> Unit

enum class PermissionType(val errorMessage: String, val permission: String) {
    SMS("Debe aceptar el permiso para continuar", Manifest.permission.RECEIVE_SMS),
    GPS_FINE("El sistema requiere acceder a su ubicación", Manifest.permission.ACCESS_FINE_LOCATION),
    GPS_COARSE("El sistema requiere acceder a su ubicación", Manifest.permission.ACCESS_COARSE_LOCATION)
}

fun Activity.validatePermissions(requestCode: Int, permissions: Array<out String>, grantResults: IntArray, type: PermissionType, validation: SmsPermissionCorrect) {
    when(type) {
        PermissionType.SMS -> {
            validation.invoke(smsPermissionValidation(requestCode, permissions, grantResults))
        }
        PermissionType.GPS_FINE, PermissionType.GPS_COARSE -> {
            validation.invoke(gpsPermissionValidation(requestCode, permissions, grantResults))
        }
    }
}

fun Activity.hasSmsPermissionRequest(fragment: Fragment? = null, isFragment: Boolean = false) : Boolean {
    val hasSmsPermission = ActivityCompat.checkSelfPermission(this, PermissionType.SMS.permission)
    return if (hasSmsPermission != PackageManager.PERMISSION_GRANTED) {
        if (!isFragment) {
            ActivityCompat.requestPermissions(this, arrayOf(PermissionType.SMS.permission), Constants.RequestCode.SMS)
        } else {
            fragment?.requestPermissions(arrayOf(PermissionType.SMS.permission), Constants.RequestCode.SMS)
        }
        false
    } else true
}

fun Activity.smsPermissionValidation(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) : Boolean {
    return if (requestCode == Constants.RequestCode.SMS) (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) else {
        this.showToast(PermissionType.SMS.errorMessage)
        return false
    }
}

fun Activity.hasGpsPermisionRequest(fragment: Fragment? = null, isFragment: Boolean = false) : Boolean {
    val hasGpsFinePermision = ActivityCompat.checkSelfPermission(this, PermissionType.GPS_FINE.permission)
    val hasGpsCoarsePermision = ActivityCompat.checkSelfPermission(this, PermissionType.GPS_COARSE.permission)
    return if (hasGpsFinePermision != PackageManager.PERMISSION_GRANTED && hasGpsCoarsePermision != PackageManager.PERMISSION_GRANTED) {
        if (!isFragment) {
            ActivityCompat.requestPermissions(this, arrayOf(PermissionType.GPS_FINE.permission, PermissionType.GPS_COARSE.permission), Constants.RequestCode.GPS)
        } else {
            fragment?.requestPermissions(arrayOf(PermissionType.GPS_FINE.permission, PermissionType.GPS_COARSE.permission), Constants.RequestCode.GPS)
        }
        false
    } else true
}

fun Activity.gpsPermissionValidation(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) : Boolean {
    return if (requestCode == Constants.RequestCode.GPS) (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) else {
        this.showToast(PermissionType.GPS_FINE.errorMessage)
        return false
    }
}