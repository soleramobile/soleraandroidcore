package solera.android.core.utils.glide

import okhttp3.OkHttpClient
import java.lang.RuntimeException
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

class NoSSLOkHttpClient {
    companion object {

        fun getNoSSLOkHttpClient() : OkHttpClient.Builder {
            try {
                val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {

                    @Throws(CertificateException::class)
                    override fun checkClientTrusted(chain: Array<out X509Certificate>?, authType: String?) {
                        //Do nothing
                    }

                    @Throws(CertificateException::class)
                    override fun checkServerTrusted(chain: Array<out X509Certificate>?, authType: String?) {
                        //Do nothing
                    }

                    override fun getAcceptedIssuers(): Array<X509Certificate> {
                        return arrayOf()
                    }
                })

                val sslContext = SSLContext.getInstance("SSL")
                sslContext.init(null, trustAllCerts, java.security.SecureRandom())

                val sslSocketFactory = sslContext.socketFactory

                val builder = OkHttpClient.Builder()
                builder.sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
                builder.hostnameVerifier { _, _ -> true }

                return builder
            } catch (ex: Exception){
                throw RuntimeException(ex)
            }
        }

    }
}