package solera.android.core.utils

import android.content.Context
import android.os.Bundle
import com.google.firebase.analytics.FirebaseAnalytics

class Analytics(context: Context) {

    private val firebaseInstance = FirebaseAnalytics.getInstance(context)

    fun sendEventWithParameters(eventName: String, parameters: Bundle) {
        firebaseInstance.logEvent(eventName, parameters)
    }

    fun setUserType(userType: String) {
        firebaseInstance.setUserProperty(Tags.type_user, userType)
    }
}

class AnalyticsContent(var eventName: String, var screenName: String? = null, var bundle: (Bundle.() -> Unit)? = null)

infix fun Context?.sendEventWithParameters(content: AnalyticsContent) {
    this?.let {
        Analytics(it).sendEventWithParameters(content.eventName, Bundle().apply {
            content.screenName?.let { validScreenName ->
                putString(Tags.screen_name, validScreenName)
            }
            content.bundle?.invoke(this)
        })
    }
}

infix fun Context?.setUserType(userType: String) {
    this?.let {
        Analytics(it).setUserType(userType)
    }
}

object Tags {
    const val screen_name : String = "screen_name"
    const val undefined   : String = "undefined"
    const val type_user   : String = "type_user"
}