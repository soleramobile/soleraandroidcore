package solera.android.core.utils

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.widget.Toast
import java.text.SimpleDateFormat
import java.util.*

//CallManager
infix fun Activity.doCall(phone: String) = CallManager.call(this, phone)

infix fun Activity?.openExternalWeb(url: String?) {
    url?.let {
        val webpage = Uri.parse(url)
        if (!url.startsWith("http://") && !url.startsWith("https://")) {
            Toast.makeText(this ?: return, Constants.Error.Message.invalidUrl, Toast.LENGTH_SHORT).show()
        }
        val intent = Intent(Intent.ACTION_VIEW, webpage)
        if (intent.resolveActivity((this ?: return).packageManager) != null) {
            this.startActivity(intent)
        }
    }
}

infix fun Activity.openMap(location: Pair<Double, Double>) = this.validateMapIntent(location)

//Date

infix fun String?.toDate(pattern: String) : Date {
    return this?.let {
        try {
            val formatter = SimpleDateFormat(pattern, Constants.Locale.Spanish)
            formatter.parse(this)
        } catch (ex: Exception) {
            ex.printStackTrace()
            Date()
        }
    } ?: Date()
}

infix fun Date.byPattern(pattern: String) : String {
    val formatter = SimpleDateFormat(pattern, Constants.Locale.Spanish)
    return formatter.format(this).capitalizeAllWords()
}